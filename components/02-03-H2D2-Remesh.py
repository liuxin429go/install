#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import fnmatch
import glob
import logging
import os
import shutil
import tarfile
import tempfile

import tools.ptf  as tl_ptf
import tools.fs   as tl_fs
import tools.call as tl_call

LOGGER = logging.getLogger("H2D2.install.H2D2-Remesh")

EXCLUDE_PATTERNS = []

def tarFilter(tarinfo):
    for ptrn in EXCLUDE_PATTERNS:
        if fnmatch.fnmatch(tarinfo.name, ptrn):
            return None
    return tarinfo

def tarH2D2 (h2d2Dir, h2d2Ver, h2d2Mpi):
    global EXCLUDE_PATTERNS
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "tarH2D2"]))

    EXCLUDE_PATTERNS.append("*.a")
    EXCLUDE_PATTERNS.append("*.la")
    EXCLUDE_PATTERNS.append("*.lib")
    EXCLUDE_PATTERNS.append("*.exe")
    EXCLUDE_PATTERNS.append("*.gz")
    EXCLUDE_PATTERNS.append("*.tgz")
    EXCLUDE_PATTERNS.append("*.bz2")
    EXCLUDE_PATTERNS.append("*.zip")
    EXCLUDE_PATTERNS.append("*.log")

    EXCLUDE_PATTERNS.append(".gitignore")
    EXCLUDE_PATTERNS.append("__pycache__")
    EXCLUDE_PATTERNS.append("*/__pycache__")

    h2d2Ptf = tl_ptf.getPlatformName()
    tarFic = "H2D2-Remesh-%s_%s_%s.tar.gz" % (h2d2Ver, h2d2Ptf, h2d2Mpi)
    LOGGER.debug("tar to %s", tarFic)
    with tl_fs.pushd(h2d2Dir):
        # One has to bypass dereference to correctly insert file symlink
        with tarfile.open(tarFic, 'w:gz', dereference=False) as tar:
            for item in glob.glob("**", recursive=True):
                if tl_fs.isSymlink(item):
                    if os.path.isfile(item):
                        tar.add(item, filter=tarFilter, recursive=False)
                else:
                    tar.add(item, filter=tarFilter, recursive=False)

    return tarFic

def doWork(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "doWork"]))

    # ---  Unlink all
    for item in reversed( glob.glob("**/", recursive=True) ):
        if item[-1] == os.sep: item = item[:-1]
        if tl_fs.isSymlink(item):
            tl_fs.deleteSymlink(item)

    # --- Get geos version
    xtrnDir = os.path.join(ctx.baseDir, "external")
    ficPtrn = os.path.join("GEOS_PTRN", "include", "GEOS_H")
    ficPtrn = ficPtrn.replace('\\', '\\\\')
    ficPtrn = ficPtrn.replace('GEOS_PTRN', 'geos-[0-9]\.[0-9]\.[0-9]')
    ficPtrn = ficPtrn.replace('GEOS_H', 'geos\.h')
    geosLst = list( tl_fs.locate(ficPtrn, root=xtrnDir) )
    if not geosLst:
        raise RuntimeError('No valid geos install could be found in %s' % xtrnDir)
    geosDir = sorted(geosLst)[-1]
    geosDir = os.path.dirname(geosDir)
    geosDir = os.path.dirname(geosDir)

    # ---  Create link
    LOGGER.debug("Create symbolic link")
    ptf = tl_ptf.getPlatformName()
    src = os.path.realpath( os.path.join("..", "bin-%s" % ptf, "bin-H2D2") )
    tl_fs.createSymlink(src, "bin")
    if tl_ptf.isWindows():
        src = os.path.join(geosDir, "build", ptf, "bin", "Release")
    else:
        src = os.path.join(geosDir, "build", ptf, "lib")
    lnk = os.path.join("bin", "common", ptf)
    tl_fs.createSymlink(src, lnk)

    # ---  tar version
    LOGGER.debug("tar H2D2-Remesh")
    tarFic = tarH2D2(".", ctx.version, ctx.MPIlib)

    # ---  Unlink all
    LOGGER.debug("Delete symbolic link")
    if tl_fs.isSymlink(lnk):   tl_fs.deleteSymlink(lnk)
    if tl_fs.isSymlink("bin"): tl_fs.deleteSymlink("bin")

    # ---  Copy to ctx.optDir
    LOGGER.debug("Copy to %s", ctx.optDir)
    shutil.copy(tarFic, ctx.optDir)

    # ---  untar
    LOGGER.debug("Untar in %s", os.path.join(ctx.optDir, "H2D2"))
    with tl_fs.pushd(ctx.optDir):
        if not os.path.isdir("H2D2"):
            os.mkdir("H2D2")
        tl_fs.untarFile(tarFic, path="H2D2")

    # ---  Log
    binDir = os.path.join(ctx.optDir, "H2D2", "bin")
    if tl_ptf.isWindows():
        print()
        print("===")
        print("You may want to modify PATH in your ENVIRONMENT")
        print("   PATH=%s;%%PATH%%" % binDir)
        if not 'TMP' in os.environ:
            print("   TMP=%s" % tempfile.gettempdir())
        print("===")
        print()
    else:
        print()
        print("===")
        print("You may want to add the following lines to your .bash_aliases")
        print("   export PATH=%s:$PATH" % binDir)
        if not 'TMP' in os.environ:
            print("   export TMP=%s" % tempfile.gettempdir())
        print("===")
        print()

def xeq(ctx):
    LOGGER.trace(".".join([os.path.splitext(__file__)[0], "xeq"]))

    # ---  Shortcut
    assert ctx.target, "ctx.target must be defined"
    if not ctx.isH2D2Remesh(): return

    # ---  Header
    LOGGER.info("H2D2 - package H2D2-Remesh")

    # ---  Pre-conditions
    assert ctx.baseDir, "ctx.baseDir must be defined"
    assert ctx.optDir,  "ctx.optDir must be defined"
    assert ctx.version, "ctx.version must be defined"

    # ---  Create stage directory
    if not os.path.isdir("_stage"):
        os.mkdir("_stage")

    with tl_fs.pushd("_stage"):
        doWork(ctx)

    # ---  Footer
    LOGGER.info("H2D2 - package H2D2-Remesh: Done")

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.install")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.TRACE)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    ctx.target = "H2D2-Remesh"
    xeq(ctx)
