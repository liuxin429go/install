#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os
import platform
import re
import shutil

import tools.call    as tl_call
import tools.ptf     as tl_ptf


LOGGER = logging.getLogger("H2D2.install.sys-install")

def is_sudo():
    try:
        # Test first with sudo -v
        # sudo -v can have falls positive
        ret = tl_call.doCallOrRaise("sudo -v", doLog=False)
        # Test then with groups
        # groups can be many things
        # not shure to catch all, be we should be on the secure side
        grps = tl_call.doCallOrRaise("groups", doLog=False)
        ret = any(g in ['admin', 'sudo', 'wheel'] for g in grps.split())
    except:
        ret = False
    return ret

def has_apt():
    return shutil.which("apt-get")

def has_yum():
    return shutil.which("which yum")

def has_zypper():
    return shutil.which("which zypper")

def installUnx(ctx):
    assert not tl_ptf.isWindows()
    assert platform.machine() == "x86_64"

    pkgs = ["libcanberra-gtk-module", "libcanberra-gtk3-module"]
    if not is_sudo():
        LOGGER.warning("You are not a sudoer")
        LOGGER.warning("  Make shure the folowing packages are installed:")
        LOGGER.warning("  %s" % ' '.join(pkgs))
        return
    if has_apt():
        installCmd = "sudo apt-get --quiet --assume-yes install"
        LOGGER.debug("install with apt-get")
    elif has_yum():
        installCmd = "sudo yum --assumeyes install"
        LOGGER.debug("install with yum")
        LOGGER.warning("install with yum NEVER tested")
    elif has_zypper():
        installCmd = "sudo zypper --quiet --non-interactive install"
        LOGGER.debug("install with zypper")
        LOGGER.warning("install with zypper NEVER tested")
    else:
        LOGGER.warning("Unknown package installer")
        LOGGER.warning("  Make shure the folowing packages are installed:")
        LOGGER.warning("  %s" % ' '.join(pkgs))
        return

    # ---  Mandatory
    if ctx.isH2D2Tools():
        LOGGER.info("Mandatory packages")
        LOGGER.info("  %s" % ' '.join(pkgs))
        tl_call.doCallOrRaise([installCmd] + pkgs)

def installWin(ctx):
    assert tl_ptf.isWindows()
    assert platform.machine() == "AMD64"


def xeq(ctx):
    # ---  Header
    LOGGER.info("H2D2 - System")

    # ---  Pre-condition
    assert ctx.target, "ctx.target must be defined"
    assert ctx.optDir, "ctx.optDir must be defined"
    assert ctx.dwnDir, "ctx.dwnDir must be defined"

    # ---  Install packages
    if tl_ptf.isWindows():
        installWin(ctx)
    else:
        installUnx(ctx)

    # ---  Footer
    LOGGER.info("H2D2 - System: Done")

if __name__ == "__main__":
    from tools.context     import getTestContext
    from tools.addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER = logging.getLogger("H2D2.install")
    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("unit test: %s", __file__)

    ctx = getTestContext()
    xeq(ctx)
