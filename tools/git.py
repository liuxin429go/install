#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

# Utility functions related to:
#   git clone and pull

import logging
import os

from .call import doCallOrRaise, CalledProcessError

LOGGER = logging.getLogger("H2D2.install.git")

def gitCloneMaster(gitUrl, gitRep, cwd=os.curdir):
    """
    git clone on master
    """
    gitPth = os.path.join(cwd, gitRep.split('/')[-1])
    if not os.path.isdir(gitPth):
        url = gitUrl + gitRep + '.git'
        cmd = 'git clone --recursive -v'
        doCallOrRaise([cmd, url], cwd=cwd)
    else:
        cmd = 'git stash push'
        doCallOrRaise(cmd, cwd=gitPth)
        cmd = 'git pull -v --progress "origin"'
        doCallOrRaise(cmd, cwd=gitPth)
        try:
            cmd = 'git stash pop'
            doCallOrRaise(cmd, cwd=gitPth)
        except CalledProcessError as e:
            pass

def gitCloneBranch(gitUrl, gitRep, gitBrn, cwd=os.curdir):
    """
    git clone on branch
    """
    gitPth = os.path.join(cwd, gitRep.split('/')[-1])
    if not os.path.isdir(gitPth):
        url = gitUrl + gitRep + '.git'
        cmd = 'git clone -b ' + gitBrn + ' --recursive -v'
        doCallOrRaise([cmd, url], cwd=cwd)
    else:
        cmd = 'git stash push'
        doCallOrRaise(cmd, cwd=gitPth)
        cmd = 'git pull -v --progress "origin"'
        doCallOrRaise(cmd, cwd=gitPth)
        try:
            cmd = 'git stash pop'
            doCallOrRaise(cmd, cwd=gitPth)
        except CalledProcessError as e:
            pass

def gitLabCloneMaster(gitRep, cwd=os.curdir):
    """
    git clone from GitLab on master
    """
    gitUrl = 'https://gitlab.com/h2d2/'
    return gitCloneMaster(gitUrl, gitRep, cwd=cwd)

def gitLabCloneBranch(gitRep, gitBrn, cwd=os.curdir):
    """
    git clone from GitHub on branch
    """
    gitUrl = 'https://gitlab.com/h2d2/'
    return gitCloneBranch(gitUrl, gitRep, gitBrn, cwd=cwd)

def gitHubCloneMaster(gitRep, cwd=os.curdir):
    """
    git clone from GitHub on master
    """
    gitUrl = 'https://github.com/h2d2-test/'
    return gitCloneMaster(gitUrl, gitRep, cwd=cwd)

def gitHubCloneBranch(gitRep, gitBrn, cwd=os.curdir):
    """
    git clone from GitHub on branch
    """
    gitUrl = 'https://github.com/h2d2-test/'
    return gitCloneBranch(gitUrl, gitRep, gitBrn, cwd=cwd)

if __name__ == "__main__":
    from context     import getTestContext
    from addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    logHndlr = logging.StreamHandler()
    FORMAT = "%(asctime)s %(levelname)s %(message)s"
    logHndlr.setFormatter( logging.Formatter(FORMAT) )

    LOGGER.addHandler(logHndlr)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.info("unit test: %s", __file__)

    url = "h2d2.build"
    gitLabCloneMaster(url, cwd='.')
