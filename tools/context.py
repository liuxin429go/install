#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

class Context:
    def __init__(self):
        self.target  = ""       # [H2D2, H2D2-tools, H2D2-Remesh, H2D2-OpenDA]
        self.branch  = ""       # branche_1810
        self.version = ""       # 1810
        self.baseDir = ""       # h2d2-1810
        self.optDir  = ""       # ~/opt
        self.dwnDir  = ""       # ~/opt
        self.complr  = ""       # [gcc, itlX64, sun]
        self.MPIlib  = ""       # [openmpi, ...]
        self.integer = ""       # [i4, i8]
        self.noGUI   = True     # 
        self.nogit   = True     # 

    def __repr__(self):
        lines = [
            "Target:                %s" % self.target,
            "Branch:                %s" % self.branch,
            "Version:               %s" % self.version,
            "Base directory:        %s" % self.baseDir,
            "Binaries directory:    %s" % self.optDir,
            "Download directory:    %s" % self.dwnDir,
            "Compiler suite:        %s" % self.complr,
            "MPI library:           %s" % self.MPIlib,
            "Fortran Integer size:  %s" % self.integer,
            # "Skip GUI app:          %s" % self.noGUI,
            # "Skip git update:       %s" % self.nogit,
        ]
        return "\n".join(lines)

    def getCompilerSuite(self):
        return self.complr.split('-')[0]
        
    def getMPISuite(self):
        return self.MPIlib.split('-')[0]
        
    def isH2D2(self):
        return self.target == "H2D2"

    def isH2D2Tools(self):
        return self.target == "H2D2-tools"

    def isH2D2Remesh(self):
        return self.target == "H2D2-Remesh"

    def isH2D2OpenDA(self):
        return self.target == "H2D2-OpenDA"
        
def getTestContext():
    import os
    ctx = Context()
    ctx.target  = "H2D2"
    ctx.branch  = "branche_1904"
    ctx.version = "dev"
    ctx.baseDir = os.path.normpath( os.path.join(os.path.split(__file__)[0], "..", "..") )
    ctx.optDir  = os.path.join(os.environ["HOME"], "opt")
    ctx.dwnDir  = ctx.optDir
    ctx.complr  = "itlX64"  # "gcc"
    ctx.MPIlib  = "msmpi"   # "openmpi"
    ctx.integer = "i4"
    ctx.noGUI   = False
    ctx.nogit   = False
    return ctx
