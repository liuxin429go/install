#!/bin/bash
#-----------------------------------------------------------------------------
# Copyright (c) INRS 2011-2017
# Institut National de la Recherche Scientifique (INRS)
#
# Distributed under the GNU Lesser General Public License, Version 3.0.
# See accompanying file LICENSE.txt.
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# The function error_exit print the error message to stderr and exits.
#
# Input:
#    $*  The message parts
#
# Output:
#-----------------------------------------------------------------------------
function error_exit
{
   echo
   for i in "$@"
   do
      echo -e $i 1>&2
   done

   echo
   read -p "Press <Enter> to continue"
   exit 2
}

#-----------------------------------------------------------------------------
# Get directory for config file
#
# Input:
#
# Output:
#    RET_1    Config file directory
#-----------------------------------------------------------------------------
function get_config_file_dir
{
   local _ROOT=.
   if [ -n "$HOME" ]; then
      _ROOT=$HOME
   else
      error_exit "Error: No HOME directory\nEnvironment Variable HOME must be defined"
   fi

   RET_1=$_ROOT/.H2D2
}

#-----------------------------------------------------------------------------
# The function split_path splits the path (1st argument) in two components:
# the filename and the root path. The scope is local.
#
# Input:
#    %1 Path

#
# Output:
#    RET_1    Filename
#    RET_2    Dir
#-----------------------------------------------------------------------------
function split_path
{
   local _NAM=`basename $1`
   local _DIR=`dirname $1`
   RET_1=$_NAM && RET_2=$_DIR
}

#-----------------------------------------------------------------------------
# Main
#-----------------------------------------------------------------------------
function main
{
   # ---  Arguments control
   if [ -z "$H2D2_KND" ]; then
      error_exit "Error: Argument error: H2D2_KND must be defined"
   fi
   if [ -z "$H2D2_CMD" ]; then
      error_exit "Error: Argument error: H2D2_CMD must be defined"
   fi

   # ---  Get installation directory
   H2D2_INST_DIR=$(readlink -f $0)
   split_path $H2D2_INST_DIR && H2D2_INST_DIR=$RET_2
   split_path $H2D2_INST_DIR && H2D2_INST_DIR=$RET_2

   # ---  Run config file
   get_config_file_dir && H2D2_CFG_FILE=$RET_1/h2d2-tools_cfg.sh
   if [ -e $H2D2_CFG_FILE ]; then
      source $H2D2_CFG_FILE
   else
      error_exit "Error: Missing h2d2-tools_cfg.sh\nPlease run h2d2-configure first"
   fi

   # ---  Set PATH
   H2D2_BSE_PATH=$H2D2_INST_DIR/$H2D2_TOOLS_RUN_PATH
   H2D2_BIN_PATH=$H2D2_INST_DIR/$H2D2_TOOLS_RUN_PATH/bin
   H2D2_DOC_PATH=$H2D2_INST_DIR/$H2D2_TOOLS_RUN_PATH/doc

   # ---  Form the command, tokens substitution
   H2D2_CMD="${H2D2_CMD//\{H2D2_BSE_PATH\}/$H2D2_BSE_PATH}"
   H2D2_CMD="${H2D2_CMD//\{H2D2_BIN_PATH\}/$H2D2_BIN_PATH}"
   H2D2_CMD="${H2D2_CMD//\{H2D2_DOC_PATH\}/$H2D2_DOC_PATH}"

   # ---  Run H2D2
   case "$H2D2_KND" in
      "exe" | "sh")
         if [ -e ${H2D2_CMD%% *} ]; then
            export LD_LIBRARY_PATH=$H2D2_BIN_PATH:$LD_LIBRARY_PATH
            $H2D2_CMD $*
         elif [ -x "$(command -v ${H2D2_CMD%% *})" ]; then
            export LD_LIBRARY_PATH=$H2D2_BIN_PATH:$H2D2_CPL_PATH:$LD_LIBRARY_PATH
            $H2D2_CMD $*
         else
            error_exit "Error: Invalid path\n$H2D2_CMD\ncould not be located\nPlease run h2d2-configure first"
         fi
         ;;
      "cmd")
         $H2D2_CMD $*
         ;;
      *)
         error_exit "Error: Unknown command kind: $H2D2_KND"
   esac

}

main $*
