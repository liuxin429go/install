#!/bin/bash
#-----------------------------------------------------------------------------
# Copyright (c) INRS 2011-2017
# Institut National de la Recherche Scientifique (INRS)
#
# Distributed under the GNU Lesser General Public License, Version 3.0.
# See accompanying file LICENSE.txt.
#-----------------------------------------------------------------------------

# ---  Initialise global variables
GLB_COUNT=0
H2D2_RUN_PTH=( )
H2D2_RUN_VER=( )

# ---  Execute main
echo H2D2-tools Configuration manager Version 1.1.4
echo

#-----------------------------------------------------------------------------
# The function error_exit print the error message to stderr and exits.
#
# Input:
#    $*  The message parts
#
# Output:
#-----------------------------------------------------------------------------
function error_exit
{
   echo
   for i in "$@"
   do
      echo -e $i 1>&2
   done

   case "$1" in
      Quit)
         exit 0
         ;;
      *)
         echo
         read -p "Press <Enter> to continue"
         exit 2
         ;;
   esac
}

#-----------------------------------------------------------------------------
# The function split_path splits the path (1st argument) in two components:
# the filename and the root path. The scope is local.
#
# Input:
#    $1 Path
#
# Output:
#    RET_1    Filename
#    RET_2    Dir
#-----------------------------------------------------------------------------
function split_path
{
   local _NAM=`basename $1`
   local _DIR=`dirname $1`
   RET_1=$_NAM && RET_2=$_DIR
}

#-----------------------------------------------------------------------------
# The function process_one_path extract the pieces of the configuration
# from the path (2nd argument). It sets global variables in the form
# of arrays. The scope is global because the arrays and the counter are
# of global scope.
#
# Input:
#    $1    The search pattern
#    $2    Input path
#    $3    H2D2 root path
#
# Output:
#    H2D2_RUN_PTH[nn]
#    H2D2_RUN_PTF[nn]
#-----------------------------------------------------------------------------
function process_one_path
{
   local _PTH
   local _NAM
   local _BLD
   local _LNK
   local _CPL
   local _MPI
   local _PTF
   local _VER

   _PTN="$1"
   _PTH="$2"
   _ROO="$3"
   _FND=0
   if [[ $_PTH != $_PTN ]]; then return 1; fi
   if [ $_FND == 0 ] && [ -x "$_PTH/DADataAnalyzer" ]    && [ -n "`file $_PTH/DADataAnalyzer    | grep ELF`" ]; then _FND=1; fi
   if [ $_FND == 0 ] && [ -f "$_PTH/DADataAnalyzer.py" ] && [ -n "`file $_PTH/DADataAnalyzer.py | grep Python`" ]; then _FND=1; fi
   if [ $_FND == 0 ]; then return 1; fi

   split_path "$_PTH" && _PTH="$RET_2"
   split_path "$_PTH" && _PTH="$RET_2"
   _NAM=${_PTH##$_ROO}

   split_path "$_PTH" && _BLD="$RET_1" && _PTH="$RET_2"
   split_path "$_PTH" && _LNK="$RET_1" && _PTH="$RET_2"
   split_path "$_PTH" && _CPL="$RET_1" && _PTH="$RET_2"
   split_path "$_PTH" && _MPI="$RET_1" && _PTH="$RET_2"
   split_path "$_PTH" && _PTF="$RET_1" && _PTH="$RET_2"
   split_path "$_PTH" && _VER="$RET_1" && _PTH="$RET_2"

   H2D2_RUN_PTH[$GLB_COUNT]=${_NAM:1}
   H2D2_RUN_VER[$GLB_COUNT]=${_VER}
   let "GLB_COUNT += 1"
}

#-----------------------------------------------------------------------------
# The function get_configurations will get all H2D2 configurations rooted at
# the path (2nd argument). It calls process_one_path for each path.
# The scope is local.
#
# Input:
#    $1    The search pattern
#    $2    Input path
#    $3    H2D2 root path (default to #1)
#
#-----------------------------------------------------------------------------
function get_configurations
{
   local _DIR
   local _ROO
   local _PTH

   _PTN="$1"
   _DIR="$2"
   if [ -n "$3" ]; then _ROO="$3"; else _ROO="$2"; fi

   for p in `ls -d "$_DIR"/*/ 2>/dev/null`
   do
      _PTH="${p: :${#p} -1}"
      process_one_path "$_PTN" "$_PTH" "$_ROO"
      if [ $? -ne 0 ]; then get_configurations "$_PTN" "$_PTH" "$_ROO"; fi
   done
}

#-----------------------------------------------------------------------------
# The function get_user_choice set-up the menu and returns the user choice. The
# menu is build from the information gathered by get_configurations.
# The scope is local.
#
# Input:
#    None
#
# Ouput:
#    RET_1    The index of the choice
#-----------------------------------------------------------------------------
function get_user_choice
{
   local _INDX
   local _CLR=("$(tput sgr0)" "$(tput setaf 136)")
   if test $(tput colors) -le 8; then
      _CLR=("$(tput sgr0)" "$(tput setaf 1)")
   fi

   printf "H2D2-tools configurations\n"
   printf "=========================\n"
   local i=0
   local j=1
   local c=0
   while [ $i -lt $GLB_COUNT ]
   do
      printf "%s[%2i] %s\n" ${_CLR[$c]} $j ${H2D2_RUN_PTH[$i]}
      let "i += 1"
      let "j += 1"
      let "c = (c+1) % 2"
   done
   printf "$(tput sgr0)\n"

   read -p "Choose the appropriate configuration (q to quit): " _INDX
   if [ -z $_INDX ];    then error_exit "Invalid choice: empty choice"; fi
   if [ -z "${_INDX##*[qQ]*}" ]; then error_exit "Quit"; fi
   if [ -z "${_INDX##*[!0-9]*}" ]; then error_exit "Invalid choice: not an integer"; fi
   if [ $_INDX -lt 1 ]; then error_exit "Invalid choice: index too low"; fi
   if [ $_INDX -gt $GLB_COUNT ]; then error_exit "Invalid choice: index too high"; fi

   let "_INDX -= 1"
   RET_1=$_INDX
}

#-----------------------------------------------------------------------------
# Get directory for config file. If the diretory does not exist, it
# will be created.
#
# Input:
#
# Output:
#    RET_1    Config file directory
#-----------------------------------------------------------------------------
function get_config_file_dir
{
   local _ROOT=.
   if [ -n "$HOME" ]; then
      _ROOT=$HOME
   else
      error_exit "Error: No HOME directory\nEnvironment Variable HOME must be defined"
   fi

   _ROOT=$_ROOT/.H2D2
   if [ ! -d "$_ROOT" ]; then `mkdir -p $_ROOT/CrashReporting/Dumps`; fi

   RET_1=$_ROOT
}

#-----------------------------------------------------------------------------
# The function write_config_file write the configuration file
# corresponding to the user choice (2nd parameter).
#
# Input:
#    $1       Configuration root directory
#    $2       Index of user choice
#
# Output:
#    None
#-----------------------------------------------------------------------------
function write_config_file
{
   local _CFG_ROOT
   local _CFG_INDX
   local _RUN_PATH

   _CFG_ROOT=$1
   _CFG_INDX=$2

   # ---  Get run path
   _RUN_PATH=${H2D2_RUN_PTH[$_CFG_INDX]}

   # ---  Write config file
   _CFG_FILE=$_CFG_ROOT/h2d2-tools_cfg.sh
   echo Writing "$_CFG_FILE"
   echo "#H2D2-tools active configuration"> "$_CFG_FILE"
   echo H2D2_TOOLS_RUN_PATH=$_RUN_PATH>> "$_CFG_FILE"
}

#-----------------------------------------------------------------------------
# Print the config file
#
# Input:
#
# Output:
#-----------------------------------------------------------------------------
function print_config_file
{
   local _CFG_ROOT
   local _CFG_FILE

   get_config_file_dir && _CFG_ROOT=$RET_1
   _CFG_FILE=$_CFG_ROOT/h2d2_cfg.sh

   while read l || [[ -n "$l" ]]; do
       echo "$l"
   done < "$_CFG_FILE"
}

#-----------------------------------------------------------------------------
# usage
#-----------------------------------------------------------------------------
function usage
{
   echo "Usage:  h2d2-tools-configure [option] PATTERN"
   echo "Options:"
   echo "        -h"
   echo "        --help   Print this help message"
   echo "        -p"
   echo "        --print  Print the current configuration"
   echo "Parameters:"
   echo "        PATTERN  Filtering pattern for the files"
   echo "                 Default to *"
}

#-----------------------------------------------------------------------------
# Main
#-----------------------------------------------------------------------------
function main
{
   local H2D2_SRCH_PTN
   local H2D2_INST_PTR
   local H2D2_CFG_ROOT
   local H2D2_CFG_INDX

   # ---  Command line parameters
   case "$1" in
   -h | --help)
      usage
      exit ;;
   -p | --print)
      print_config_file
      exit ;;
   "")
      H2D2_SRCH_PTN=\* ;;
   *)
      H2D2_SRCH_PTN=\*"$1"\* ;;
   esac

   # ---  Get installation directory
   H2D2_INST_DIR=$(readlink -f "$0")
   split_path "$H2D2_INST_DIR" && H2D2_INST_DIR="$RET_2"
   split_path "$H2D2_INST_DIR" && H2D2_INST_DIR="$RET_2"

   # ---  Process
   get_configurations "$H2D2_SRCH_PTN" "$H2D2_INST_DIR"
   get_config_file_dir  && H2D2_CFG_ROOT="$RET_1"
   get_user_choice      && H2D2_CFG_INDX="$RET_1"
   write_config_file "$H2D2_CFG_ROOT" "$H2D2_CFG_INDX"
}

main $*

