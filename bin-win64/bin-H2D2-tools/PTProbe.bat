::-----------------------------------------------------------------------------
:: Copyright (c) INRS 2011-2017
:: Institut National de la Recherche Scientifique (INRS)
::
:: Distributed under the GNU Lesser General Public License, Version 3.0.
:: See accompanying file LICENSE.txt.
::-----------------------------------------------------------------------------
@echo off
setlocal EnableExtensions EnableDelayedExpansion

:: --- Argument for h2d2-launch.bat
set H2D2_KND=bat
set H2D2_CMD={H2D2_BIN_PATH}/PTProbe.bat

:: --- Execute via h2d2-launch.bat
set LCL_PTH=%~dp0
"%LCL_PTH%h2d2-tools-launch.bat" %*
