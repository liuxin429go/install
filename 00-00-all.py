#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

# ---  Detect Python2
from __future__ import print_function   # At top of script
import sys
if sys.version_info[0] < 3:
    print("Python version 3 required. Please rerun with python3:")
    print("   python3 %s" % __file__)
    exit(1)

# ---  System imports
import argparse
import glob
import importlib
import logging
import logging.handlers
import os
import re
import traceback

# ---  Our imports
from tools.addLogLevel import addLoggingLevel
from tools.context     import Context
from tools.fs          import getDownloadPath, pushd
from tools.ptf         import isWindows

# ---  Define HOME
try:
    HOME = os.environ["HOME"]
except KeyError:    # Possible on Windows
    HOME = os.environ["HOMEDRIVE"] + os.environ["HOMEPATH"]

if isWindows():
    OPTDIR = os.environ["APPDATA"]
    DWNDIR = getDownloadPath()
else:
    OPTDIR = os.path.join(HOME, "opt")
    DWNDIR = os.path.join(HOME, "opt")

    
def getUserConfirmation(ctx):
    """
    Get user confirmation for the context with handling of
    default value. In case of script mode, i.e. non 
    interactive mode, return 'y'
    """
    if sys.stdout != sys.__stdout__: return 'y'

    if LOGGER.level > logging.INFO:
        print("Compiling with context:")
        for item in repr(ctx).split("\n"):
            print("   %s" % item)

    print("Proceed? [Y/n] ", end="")
    s = input()
    if not s:
        s = 'y'
    elif s in ['Y', 'y', 'N', 'n']:
        s = s.lower()
    else:
        s = None
    return s

class ArgWithVersion:
    def __init__(self, choices):
        self.choices = choices
    def __call__(self, s):
        if s in self.choices: 
            return s
        p_maj = r'([1-9][0-9]*)'
        p_min = r'([0-9]+)'
        p_ver = r'({vmaj:s}(\.{vmin:s}*))?'.format(vmaj = p_maj, vmin = p_min)
        p_glb = r'\w+-{ver:s}'.format(ver = p_ver)
        pat = re.compile(p_glb)
        if not pat.match(s):
            raise argparse.ArgumentTypeError
        return s

def parseArgs():
    """
    Parse the arguments.

    Note:
     arguments without "choices" should have an empty metavar to block
     the ugly use of "dest" in the help message.
     arguments with "choices" will display the list, which is OK.
    """
    class Formatter(argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter): 
        pass
    fmtCls = Formatter
    parser = argparse.ArgumentParser(__file__, formatter_class=fmtCls)

    parser.add_argument("--build",
                        help    = '\n'.join(
                                  [
                                    "Build target",
                                    "",
                                  ]),
                        choices = ["H2D2",
                                   "H2D2-tools",
                                   "H2D2-Remesh",
                                   ],
                        default = "H2D2",
                        dest    = "h2d2Tgt",
                        required= True)

    #parser.add_argument("--branch",
    #                    help    = '\n'.join(
    #                              [
    #                                "git branch to build",
    #                                "",
    #                              ]),
    #                    default = "branche_1904",
    #                    dest    = "h2d2Brn",
    #                    metavar = "")

    parser.add_argument("--version",
                        help    = '\n'.join(
                                  [
                                    "Version name",
                                    "",
                                  ]),
                        default = "1904",
                        dest    = "h2d2Ver",
                        metavar = "")

    cwd = os.getcwd()
    pth = os.path.dirname(__file__)
    if os.path.abspath(cwd) == os.path.abspath(pth):
        cwd = os.path.join(cwd, "..")
    cwd = os.path.normpath(cwd)
    parser.add_argument("--root-dir",
                        help    = '\n'.join(
                                  [
                                    "Root build directory",
                                    "",
                                  ]),
                        default = cwd,
                        dest    = "h2d2Dir",
                        metavar = "")

    helpUnx = "opt directory for Conda(Python), mpi, H2D2 binaries"
    helpWin = "opt directory for H2D2 binaries"
    parser.add_argument("--opt-dir",
                        help    = '\n'.join(
                                  [
                                    helpWin if isWindows() else helpUnx,
                                    "",
                                  ]),
                        default = OPTDIR,
                        dest    = "h2d2Opt",
                        metavar = "")

    parser.add_argument("--download-dir",
                        help    = '\n'.join(
                                  [
                                    "Download directory",
                                    "",
                                  ]),
                        default = DWNDIR,
                        dest    = "h2d2Dwn",
                        metavar = "")

    choices = ["msmpi", "intelmpi", "_none_"] if isWindows() else ["openmpi", "intelmpi", "_none_"]
    default = "intelmpi" if isWindows() else "openmpi"
    parser.add_argument("--mpi-lib",
                        help    = '\n'.join(
                                  [
                                    "MPI system, possibly with version",
                                    "%s" % choices,
                                  ]),
                        # choices = choices,
                        default = default,
                        type    = ArgWithVersion(choices),
                        dest    = "h2d2Mpi",
                        metavar = "")

    #parser.add_argument("--integer-size",
    #                    help    = '\n'.join(
    #                              [
    #                                "Fortran default integer size",
    #                                "",
    #                              ]),
    #                    choices = [4, 8],
    #                    default = 4,
    #                    type    = int,
    #                    dest    = "h2d2Int")

    choices = ["itlX64", "gcc", "cython", "pyinstaller", "_none_"] if isWindows() else ["gcc", "sun", "itlX64", "cmcitl", "cython", "pyinstaller", "_none_"]
    default = "itlX64" if isWindows() else "gcc"
    parser.add_argument("--compiler-suite",
                        help    = '\n'.join(
                                  [
                                    "compiler suite to use, possibly with version",
                                    "%s" % choices,
                                  ]),
                        # choices = choices,
                        default = default,
                        type    = ArgWithVersion(choices),
                        dest    = "h2d2Cpl",
                        metavar = "")

    parser.add_argument("--log-level-file",
                        help    = '\n'.join(
                                  [
                                    "log level for log file",
                                    "",
                                  ]),
                        choices = ["critical", "error", "warning", "info", "dump", "debug", "trace"],
                        default = "debug",
                        dest    = "logLvlFic")

    parser.add_argument("--log-level-terminal",
                        help    = '\n'.join(
                                  [
                                    "log level for interactive terminal output",
                                    "",
                                  ]),
                        choices = ["critical", "error", "warning", "info", "dump", "debug", "trace"],
                        default = "info",
                        dest    = "logLvlStr")

    parser.add_argument("--stages",
                        help    = '\n'.join(
                                  [
                                    "stages to build: space separated int as in  '2 3'",
                                    "   1: conda and system install",
                                    "   2: environment",
                                    "   3: download source files",
                                    "   4: build external libraries",
                                    "   5: build target",
                                    "",
                                  ]),
                        nargs   = "*",
                        type    = int,
                        default = [],
                        dest    = "stgs",
                        metavar = "")

    parser.add_argument("--only",
                        help    = '\n'.join(
                                  [
                                    "list of packages to build: Python list of string",
                                    "   *: for all",
                                    "",
                                  ]),
                        nargs   = "*",
                        default = [],
                        dest    = "pkgs",
                        metavar = "")

    parser.add_argument("--yes",
                        help    = "Do not ask for confirmation",
                        action  = "store_true",
                        dest    = "skipConfirm")

    args, unknowns = parser.parse_known_args()
    if unknowns:
        LOGGER.warning("Skipping unrecognized arguments:")
        for arg in unknowns:
            LOGGER.warning("   %s", arg)
        LOGGER.warning("")

    return args

def doOneStage(ctx, glPtrn, pkgs):
    """
    Execute all scripts corresponding to the glob pattern glPtrn
    """

    # ---  Loop on all scripts
    scripts = [ f for f in glob.glob(os.path.join("components", glPtrn)) ]
    for script in sorted(scripts):
        if pkgs and not any( [ p in script for p in pkgs ] ): continue
        
        mdlLvl  = int( os.path.basename(script).split('-')[0] )
        mdlName = os.path.splitext(script)[0]
        mdlName = '.'.join( os.path.split(mdlName) )
        mdl = None
        try:
            LOGGER.debug("importing %s", mdlName)
            mdl = importlib.import_module(mdlName)
        except Exception as e:
            errMsg = "Exception\n%s\n%s" % (str(e), traceback.format_exc())
            LOGGER.error("Could not load module %s", mdlName)
            LOGGER.error(errMsg)
            if mdlLvl <= 2: raise
        else:
            try:
                mdl.xeq(ctx)
            except Exception as e:
                if mdlLvl <= 2: raise
                errMsg = "Skipping non fatal exception\n%s\n%s" % (str(e), traceback.format_exc())
                LOGGER.error(errMsg)

def main():
    """
    Parse the arguments
    Execute each script in directory components
    """

    # ---  Log arguments to file only
    LOGGER.handlers[0].setLevel(logging.INFO)
    LOGGER.handlers[1].setLevel(logging.CRITICAL)
    LOGGER.info("Command line:")
    LOGGER.info("   %s" % ' '.join(sys.argv))
    LOGGER.info("")

    # ---  Parse the arguments
    args = parseArgs()

    # ---  Set the log levels
    lvls = {"critical": logging.CRITICAL,
            "error"   : logging.ERROR,
            "warning" : logging.WARNING,
            "info"    : logging.INFO,
            "dump"    : logging.DUMP,
            "debug"   : logging.DEBUG,
            "trace"   : logging.TRACE}
    LOGGER.handlers[0].setLevel(lvls[args.logLvlFic])
    LOGGER.handlers[1].setLevel(lvls[args.logLvlStr])

    # ---  Context
    ctx = Context()
    ctx.target  = args.h2d2Tgt
    ctx.branch  = "---"
    ctx.version = args.h2d2Ver
    ctx.baseDir = os.path.abspath( args.h2d2Dir )
    ctx.optDir  = os.path.abspath( args.h2d2Opt )
    ctx.dwnDir  = os.path.abspath( args.h2d2Dwn )
    ctx.complr  = args.h2d2Cpl
    ctx.MPIlib  = args.h2d2Mpi
    ctx.integer = "---"
    # ---  Stages
    stgs = range(10) if not args.stgs else args.stgs
    pkgs = args.pkgs

    # ---  Get confirmation
    LOGGER.info("Installing with context:")
    for item in repr(ctx).split("\n"):
        LOGGER.info("   %s", item)
    LOGGER.info("For stages:")
    LOGGER.info("   %s", stgs)
    LOGGER.info("For packages:")
    LOGGER.info("   %s", pkgs)
    if not args.skipConfirm:
        r = getUserConfirmation(ctx)
        if not r:
            print('Invalid answer, aborting')
            exit(1)
        elif r == 'n':
            exit(1)

    # ---  Loop on all scripts except 00-00
    wdir = os.path.dirname(__file__)
    with pushd(wdir if wdir else '.'):
        for stg in stgs:
            glPtrn = "%02d-[0-9][0-9]-*.py" % stg
            doOneStage(ctx, glPtrn, pkgs)

    # ---  Footer
    LOGGER.info("H2D2 - Compile: Done")

if __name__ == "__main__":
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    strHndlr = logging.StreamHandler()
    FORMAT = "[%(levelname)-8.8s] %(message)s"
    strHndlr.setFormatter( logging.Formatter(FORMAT) )
    strHndlr.setLevel(logging.INFO)

    ficName  = "h2d2-install.log"
    ficHndlr = logging.handlers.RotatingFileHandler(ficName, mode='a', encoding='utf-8', backupCount=5)
    if os.path.isfile(ficName):
        ficHndlr.doRollover()
    FORMAT = "%(asctime)s [%(levelname)-8.8s] %(message)s"
    ficHndlr.setFormatter( logging.Formatter(FORMAT) )
    ficHndlr.setLevel(logging.DEBUG)

    LOGGER = logging.getLogger("H2D2.install")
    LOGGER.addHandler(ficHndlr)
    LOGGER.addHandler(strHndlr)
    LOGGER.setLevel(logging.TRACE)

    main()
